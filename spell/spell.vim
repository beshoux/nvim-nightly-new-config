"pressez "z=" et vous entrez dans le mode "choix de mot" - taper Enter ou le numéro du mot que vous voulez utiliser.
"presser "zg" pour ajouter le mot dans le dictionnaire local
"presser "]s" pour aller au prochain mot
"presser "[s" pour aller au prochain mot
"Ce procédé est sympa pour une phase finale, après avoir rédigé. Mais si vous voulez avoir une mode de correction en cours de frappe, 
"il existe une autre commande. Vim permet de compléter un mot lorsque l'on s'en sert d'éditeur de code. On utilise alors 
"CTRL+X puis une lettre correspondant à un mode de complétion.
"Le mode utilisé pour "spell" est "s". Donc, après avoir tapé un mot de la mauvaise manière, il suffit, sans quitter le mode "insert", 
"de presser CTRL+X puis "s". Une liste de propositions de mots s'affiche. Sélectionnez celui qui vous plait, puis pressez "espace" 
"ou tout autre caractère pour continuer la rédaction. 
let g:languagetool_jar='languagetool.jar'

nnoremap <Leader>wn ]s
nnoremap <Leader>wp [s
nnoremap <Leader>wN G[s
nnoremap <Leader>wP gg]s
nnoremap <Leader>wf 1zg
nnoremap <Leader>we 2zg

" La vérification orthographique s’activera. Une autre pression sur ces mêmes touches désactivera cette dernière.
nnoremap <Leader>ts :setlocal spell! spell?<Return>
